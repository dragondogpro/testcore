﻿using System;

namespace ClassLibrary1
{
    public class Class1
    {

        public int Sum(int x, int y)
        {
            return x + y;
        }

        public int Mult(int x, int y)
        {
            return x * y;
        }

        public int Sub(int x, int y)
        {
            return x - y;
        }

    }
}
